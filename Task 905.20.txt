#Task 905.20 Where clause

# Lấy danh sách khách hàng (customers) có first_name = ‘Alexander’
select * from customers where first_name = 'Alexander'

#Lấy danh sách khách hàng có sales_rep_employee_number > 1700
select * from customers where sales_rep_employee_number > 1700

#Lấy danh sách khách hàng có sales_rep_employee_number nằm trong đoạn từ 1000 đến 1500 theo 2 cách
select * from customers where sales_rep_employee_number >=1000 and sales_rep_employee_number <=1500
select * from customers where sales_rep_employee_number between 1000 and 1500

#Lấy danh sách khách hàng có id nằm trong danh sách 201, 202, 204, 233, 255
select * from customers where id in (201, 202, 204, 233, 255)

#Lấy danh sách các khách hàng có first_name bắt đầu bằng chữ L
select * from customers where first_name like 'L%'

#Lấy danh sách các khách hàng có first_name bắt đầu bằng chữ A và sống tại country Russia
select * from customers where first_name like 'A%' and country = 'Russia'
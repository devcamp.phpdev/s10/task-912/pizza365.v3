<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PizzaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1; $i <= 10; $i ++) {
            \DB::table('pizzas')->insert([
                'name' => "Pizza name {$i}",
                'description' => "Pizza description {$i}"
            ]);
        }    
    }
}
